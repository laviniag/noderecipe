var express = require('express'),
	path = require('path'),
	bodyParser = require('body-parser'),
	cons = require('consolidate'),
	dust = require('dustjs-helpers'),
	mysql = require('mysql'),
	app = express();

// DB Connect String
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'portfolio'
});
connection.connect();

// Assign Dust Engine To .dust Files
app.engine('dust', cons.dust);

// Set Default Ext .dust
app.set('view engine', 'dust');
app.set('views', __dirname + '/views');

// Set Public Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function(req, res){
    // pg.connect(connect, function(err, client, done) {
	// if(err) {
	// 	return console.error('error fetching client from pool', err);
	// }
	connection.query('SELECT * FROM recipes', function(err, rows, fields){
        if(err) throw err;
		res.render('index', {recipes: rows});
		// done();
	});
});

app.post('/add',function(req, res){
    connection.query("INSERT INTO recipes (name, ingredients, directions) VALUES(?)",
    [[req.body.name, req.body.ingredients, req.body.directions]], function(err, result) {
        if(err) throw err;
		console.log('Success: '+result);
	});

	res.redirect('/');
});

app.post('/edit',function(req, res){
    connection.query("UPDATE recipes SET name=?, ingredients=?, directions=? WHERE id ="+ req.body.id,
    [req.body.name, req.body.ingredients, req.body.directions], function(err, result) {
        if(err) throw err;
		console.log('Updated: '+result);
	});

	res.redirect('/');
});

app.delete('/delete/:id',function(req, res){
    connection.query("DELETE FROM recipes WHERE id ="+ req.params.id, function(err, result) {
        if(err) throw err;
		console.log('Deleted: '+result);
	});

	res.sendStatus(200);
});

// Server
app.listen(3000, function(){
	console.log('Server Started On Port 3000');
});